<?php

namespace Drupal\koality_layout_builder_layouts;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Layout\LayoutDefault;
use Drupal\Core\Plugin\PluginFormInterface;

class KoalityLayoutBase extends LayoutDefault implements PluginFormInterface {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'margin_top' => '',
      'margin_bottom' => '',
      'overlay_top' => '',
      'overlay_bottom' => '',
      'layout_width' => 'wide',
      'remove_top_bottom_padding' => 0,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $configuration = $this->getConfiguration();

    $form['layout_width'] = [
      '#type' => 'select',
      '#title' => $this->t('Width'),
      '#description' => $this->t('How wide should the content in this layout stretch?'),
      '#default_value' => $configuration['layout_width'],
      '#options' => [
        'full-width' => $this->t('Full Width'),
        'row' => $this->t('Wide'),
        'inner-row' => $this->t('Constrained'),
      ],
    ];

    $form['remove_side_gutters'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Should gutters on the side of the layout be removed'),
      '#description' => $this->t('If checked the side gutters will be removed allowing for content to touch the edge of the layout.'),
      '#default_value' => $configuration['remove_side_gutters'],
    ];

    $form['padding_options'] = [
      '#type' => 'details',
      '#title' => $this->t('Section Padding'),
      'description' => [
        '#type' => 'item',
        '#title' => $this->t('These options are for controlling the spacing at the top and bottom of the section')
      ],
      'margin_top' => [
        '#type' => 'select',
        '#title' => $this->t('Additional Top Padding'),
        '#description' => $this->t('Should there be additional padding at the top of this section?'),
        '#default_value' => $configuration['margin_top'],
        '#options' => [
          '' => '- None -',
          'margin-top-light' => $this->t('Light'),
          'margin-top-heavy' => $this->t('Heavy'),
        ]
      ],
      'margin_bottom' => [
        '#type' => 'select',
        '#title' => $this->t('Additional Bottom Padding'),
        '#description' => $this->t('Should there be additional padding at the bottom of this section?'),
        '#default_value' => $configuration['margin_bottom'],
        '#options' => [
          '' => '- None -',
          'margin-bottom-light' => $this->t('Light'),
          'margin-bottom-heavy' => $this->t('Heavy'),
        ]
      ],
      'remove_top_bottom_padding' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Remove the top/bottom padding'),
        '#description' => $this->t('Should the regular padding at the top and bottom of the content be removed?'),
        '#default_value' => $configuration['remove_top_bottom_padding'],
      ],
    ];

    return $form;
  }

  /**
   * @inheritdoc
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    // any additional form validation that is required
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['margin_top'] = $form_state->getValue(['padding_options', 'margin_top']);
    $this->configuration['margin_bottom'] = $form_state->getValue(['padding_options', 'margin_bottom']);
    $this->configuration['remove_top_bottom_padding'] = $form_state->getValue(['padding_options', 'remove_top_bottom_padding']);
    $this->configuration['overlay_top'] = $form_state->getValue(['overlap_options', 'overlay_top']);
    $this->configuration['overlay_bottom'] = $form_state->getValue(['overlap_options', 'overlay_bottom']);
    $this->configuration['layout_width'] = $form_state->getValue('layout_width');
    $this->configuration['remove_side_gutters'] = $form_state->getValue('remove_side_gutters');
  }

  /**
   * {@inheritdoc}
   */
  public function build(array $regions) {
    // Ensure $build only contains defined regions and in the order defined.
    $build = [];
    foreach ($this->getPluginDefinition()->getRegionNames() as $region_name) {
      if (array_key_exists($region_name, $regions)) {
        $build[$region_name] = $regions[$region_name];
      }
    }
    $build['#settings'] = $this->getConfiguration();
    $build['#layout'] = $this->pluginDefinition;
    $build['#theme'] = $this->pluginDefinition->getThemeHook();
    if ($library = $this->pluginDefinition->getLibrary()) {
      $build['#attached']['library'][] = $library;
    }

    $route_match = \Drupal::routeMatch();
    $node = $route_match->getParameter('node');
    $build['#settings']['node'] = $node;

    return $build;
  }

}
