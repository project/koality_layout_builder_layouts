<?php

namespace Drupal\koality_layout_builder_layouts;

use Drupal\Core\Form\FormStateInterface;

class ThreeCenterRegions extends KoalityLayoutBase {
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
        'layout_bg_color' => '',
        'no_region_padding' => 0,
        'slider_on_mobile' => 0,
      ];
  }

  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $configuration = $this->getConfiguration();

    $form['layout_color'] = [
      '#type' => 'select',
      '#title' => $this->t('Layout Background Color'),
      '#description' => $this->t('If you select a value here, this color will display behind the entire section.'),
      '#default_value' => $configuration['layout_color'],
      '#options' => [
        '' => '- None -',
        'gray-background' => $this->t('Gray Background'),
        'medium-gray-background' => $this->t('Medium Gray Background'),
        'dark-gray-background' => $this->t('Dark Gray Background'),
        'white-background' => $this->t('White Background'),
      ],
    ];

    $form['left_region_color'] = [
      '#type' => 'select',
      '#title' => $this->t('Left Region Background Color'),
      '#description' => $this->t('If you select a value here, this color will display behind the region.'),
      '#default_value' => $configuration['left_region_color'],
      '#options' => [
        '' => '- None -',
        'gray-background' => $this->t('Gray Background'),
        'medium-gray-background' => $this->t('Medium Gray Background'),
        'dark-gray-background' => $this->t('Dark Gray Background'),
        'white-background' => $this->t('White Background'),
      ],
    ];

    $form['center_region_color'] = [
      '#type' => 'select',
      '#title' => $this->t('Center Region Background Color'),
      '#description' => $this->t('If you select a value here, this color will display behind the region.'),
      '#default_value' => $configuration['center_region_color'],
      '#options' => [
        '' => '- None -',
        'gray-background' => $this->t('Gray Background'),
        'medium-gray-background' => $this->t('Medium Gray Background'),
        'dark-gray-background' => $this->t('Dark Gray Background'),
        'white-background' => $this->t('White Background'),
      ],
    ];

    $form['right_region_color'] = [
      '#type' => 'select',
      '#title' => $this->t('Right Region Background Color'),
      '#description' => $this->t('If you select a value here, this color will display behind the region.'),
      '#default_value' => $configuration['right_region_color'],
      '#options' => [
        '' => '- None -',
        'gray-background' => $this->t('Gray Background'),
        'medium-gray-background' => $this->t('Medium Gray Background'),
        'dark-gray-background' => $this->t('Dark Gray Background'),
        'white-background' => $this->t('White Background'),
      ],
    ];

    $form['no_region_padding'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('No padding between regions.'),
      '#description' => $this->t('If checked there will be no padding between regions.'),
      '#default_value' => $configuration['no_region_padding'],
    ];

    $form['slider_on_mobile'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Slider on Mobile?'),
      '#description' => $this->t('If checked, the regions will become a slider on mobile.'),
      '#default_value' => $configuration['slider_on_mobile'],
    ];

    return parent::buildConfigurationForm($form, $form_state);
  }

  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['right_region_color'] = $form_state->getValue('right_region_color');
    $this->configuration['center_region_color'] = $form_state->getValue('center_region_color');
    $this->configuration['left_region_color'] = $form_state->getValue('left_region_color');
    $this->configuration['layout_color'] = $form_state->getValue('layout_color');
    $this->configuration['no_region_padding'] = $form_state->getValue('no_region_padding');
    $this->configuration['slider_on_mobile'] = $form_state->getValue('slider_on_mobile');

    parent::submitConfigurationForm($form, $form_state);
  }
}
