<?php

namespace Drupal\koality_layout_builder_layouts;

use Drupal\Core\Form\FormStateInterface;

class TwoCenterRegions extends KoalityLayoutBase {
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
        'region_color' => '',
        'layout_color' => '',
        'gutters' => 0,
        'stack' => 1,
        'resizer' => '50/50',
      ];
  }

  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $configuration = $this->getConfiguration();

    $form['layout_color'] = [
      '#type' => 'select',
      '#title' => $this->t('Layout Background Color'),
      '#description' => $this->t('If you select a value here, this color will display behind the entire layout.'),
      '#default_value' => $configuration['layout_color'],
      '#options' => [
        '' => '- None -',
        'gray-background' => $this->t('Gray Background'),
        'medium-gray-background' => $this->t('Medium Gray Background'),
        'dark-gray-background' => $this->t('Dark Gray Background'),
        'white-background' => $this->t('White Background'),
      ],
    ];

    $form['left_region_color'] = [
      '#type' => 'select',
      '#title' => $this->t('Left Region Background Color'),
      '#description' => $this->t('If you select a value here, this color will display behind the region.'),
      '#default_value' => $configuration['left_region_color'],
      '#options' => [
        '' => '- None -',
        'gray-background' => $this->t('Gray Background'),
        'medium-gray-background' => $this->t('Medium Gray Background'),
        'dark-gray-background' => $this->t('Dark Gray Background'),
        'white-background' => $this->t('White Background'),
      ],
    ];

    $form['right_region_color'] = [
      '#type' => 'select',
      '#title' => $this->t('Right Region Background Color'),
      '#description' => $this->t('If you select a value here, this color will display behind the region.'),
      '#default_value' => $configuration['right_region_color'],
      '#options' => [
        '' => '- None -',
        'gray-background' => $this->t('Gray Background'),
        'medium-gray-background' => $this->t('Medium Gray Background'),
        'dark-gray-background' => $this->t('Dark Gray Background'),
        'white-background' => $this->t('White Background'),
      ],
    ];

    $form['gutters'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Should have gutters?'),
      '#description' => $this->t('If checked the regions will have spacing between them.'),
      '#default_value' => $configuration['gutters'],
    ];

    $form['stack'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Stack on mobile'),
      '#description' => $this->t('If checked, on mobile this will stack vertical.'),
      '#default_value' => $configuration['stack'],
    ];

    $form['grid'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Grid Mode'),
      '#description' => $this->t('If checked, each item will display in an equal height grid to the column next to it. (will only display 50/50 grid)'),
      '#default_value' => $configuration['grid'],
    ];

    $form['resizer'] = [
      '#type' => 'select',
      '#title' => $this->t('Change Column Widths'),
      '#description' => $this->t('If you don\'t want 50/50 but want a differnt size column you can choose that here.'),
      '#default_value' => $configuration['resizer'],
      '#options' => [
        '50/50' => '50/50',
        '80/20' => '80/20',
        '20/80' => '20/80',
        '75/25' => '75/25',
        '25/75' => '25/75',
        '70/30' => '70/30',
        '30/70' => '30/70',
        '33/66' => '33/66',
        '66/33' => '66/33'
      ],
    ];

    return parent::buildConfigurationForm($form, $form_state);
  }

  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['right_region_color'] = $form_state->getValue('right_region_color');
    $this->configuration['left_region_color'] = $form_state->getValue('left_region_color');
    $this->configuration['layout_color'] = $form_state->getValue('layout_color');
    $this->configuration['gutters'] = $form_state->getValue('gutters');
    $this->configuration['stack'] = $form_state->getValue('stack');
    $this->configuration['grid'] = $form_state->getValue('grid');
    $this->configuration['resizer'] = $form_state->getValue('resizer');

    parent::submitConfigurationForm($form, $form_state);
  }
}
