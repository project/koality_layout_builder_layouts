<?php

namespace Drupal\koality_layout_builder_layouts;

use Drupal\Core\Form\FormStateInterface;

class OneCenterRegion extends KoalityLayoutBase {
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'region_color' => '',
      'layout_color' => '',
    ];
  }

  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $configuration = $this->getConfiguration();

    $form['layout_color'] = [
      '#type' => 'select',
      '#title' => $this->t('Layout Background Color'),
      '#description' => $this->t('If you select a value here, this color will display behind the entire layout.'),
      '#default_value' => $configuration['layout_color'],
      '#options' => [
        '' => '- None -',
        'gray-background' => $this->t('Gray Background'),
        'medium-gray-background' => $this->t('Medium Gray Background'),
        'dark-gray-background' => $this->t('Dark Gray Background'),
        'white-background' => $this->t('White Background'),
        'white-opacity-background' => $this->t('White Background (80% Opacity)'),
      ],
    ];

    $form['region_color'] = [
      '#type' => 'select',
      '#title' => $this->t('Region Background Color'),
      '#description' => $this->t('If you select a value here, this color will display behind the region.'),
      '#default_value' => $configuration['region_color'],
      '#options' => [
        '' => '- None -',
        'gray-background' => $this->t('Gray Background'),
        'medium-gray-background' => $this->t('Medium Gray Background'),
        'dark-gray-background' => $this->t('Dark Gray Background'),
        'white-background' => $this->t('White Background'),
        'white-opacity-background' => $this->t('White Background (80% Opacity)'),
      ],
    ];

    return parent::buildConfigurationForm($form, $form_state);
  }

  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['region_color'] = $form_state->getValue('region_color');
    $this->configuration['layout_color'] = $form_state->getValue('layout_color');

    parent::submitConfigurationForm($form, $form_state);
  }
}
